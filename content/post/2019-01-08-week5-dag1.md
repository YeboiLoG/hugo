---
title: Eerste dag na de kerstvakantie
categories: ["designchallenge"]
tags: ["onepager""presentatie""vergadering"]
date: 2019-01-08
---
Vandaag was de eerste dag na de kerstvakantie. We zijn de dag begonnen met een presentatie, waarna we een teamvergadering hebben gehouden. We hebben besproken wat we willen doen en hoe we de komende weken gaan aanpakken. Daarna heb ik mijn onepager laten voorzien van feedback. Deze feedback heb ik genoteerd.
