---
title: werken aan presentatie
categories: ["presentatie"]
tags: ["presentatie" "woonstad"]
date: 2018-10-18
---
Vandaag zijn we samengekomen om de presentatie voor te bereiden voor Woonstad. Ik heb het storyboard gemaakt van een van onze concepten. Ook heb ik de scenario’s daarvoor geschreven.
