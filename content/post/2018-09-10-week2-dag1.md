---
title: Teamuitje
categories: ["teamuitje"]
tags: ["teamuitje" "korea"]
date: 2018-09-10
---
Vandaag was het tijd voor ons teamuitje. Op de planning stond een Korean BBQ bij Seoul Sista te Rotterdam en we hebben verzameld bij Wunderbar. Nadat we een biertje hadden gedronken liepen we richting het restaurant. Daar hebben we heerlijk kunnen dineren en hebben we elkaar beter leren kennen. Als afsluiting hebben we nog een biertje gedronken bij NRC.
