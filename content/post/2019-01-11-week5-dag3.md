---
title: Prototype Party
categories: ["prototypeparty"]
tags: ["prototypeparty" "feedback"]
date: 2018-01-11
---
Vandaag stond de prototype party op het programma. We hebben voorafgaand aan de prototype party nog even afgesproken om het een en ander af te maken. Daarna zijn we naar de prototype parties gegaan. De eerste ronde was van Rolf en de tweede was van Jantien. We hebben veel feedback ontvangen waar we verder mee kunnen gaan.