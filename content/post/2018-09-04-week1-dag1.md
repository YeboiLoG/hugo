---
title: Kwartaal 1 week 1 dag 1
categories: ["kick-off"]
tags: ["kick-off"]
date: 2018-09-04
---

Vandaag zijn we begonnen met een kick off. Tijdens deze kick off kregen we met alle klassen een briefing te horen over ons aankomende project (woonstad Rotterdam). Vervolgens zijn we in de klas gaan zitten en was het tijd om de klas in groepen te verdelen. Dit deden we door de teamcaptains een soort kwartet te laten spelen terwijl de rest van de klas werd verdeeld over de teamrollen.
Daarna zijn de groepjes verdeeld door de teamcaptains en was het tijd om een voorstelronde te doen met je groepje.
Als vervolg daarop hebben we een lijst gemaakt met teamwaarden en hierop hebben onze planning van een teamuitje gebaseerd.
Als afsluiting hebben we met het team een biertje gedronken naast de school.
